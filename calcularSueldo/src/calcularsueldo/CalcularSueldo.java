package calcularsueldo;

public class CalcularSueldo {
    public static void main(String[] args) { 
        //Necesitamos las categorias
        //Vaviables
        
        //Plus salarial
        float plus;
        //Porcentaje
        float porcentaje = 1.05f;
        
        
        //Nombre del empleado
        String nombre = "Jose Estevez";
        
        //Valor de la hora
        float valorHora = 300;
        
        //Cantidad total de horas mensuales
        int horasTrabajadas = 160;

        //Calculo del sueldo. Valor hora multiplicado por la cantidad total de horas.
        float sueldo = valorHora * horasTrabajadas;
        
        //Categoria por defecto
        String categoria = "A";
        
        //Re asignamos o no la categoria
        if (sueldo>40000) {
            categoria = "B";
        }
        
        if (categoria.equals("B")) {
            sueldo = sueldo * porcentaje;
        }
        
        
        System.out.println(nombre + " tiene un sueldo de " + sueldo + " y es de categoría " + categoria);
    }    
}
